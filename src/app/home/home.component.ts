import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  registerMode = false;
  registerForm = true;
  jwtHelper = new JwtHelperService();
  constructor() { }

  ngOnInit(): void {
    const token = localStorage.getItem('token');
    this.registerForm = this.jwtHelper.isTokenExpired(token);
  }

  registerToggle(): void{
    this.registerMode = true;
  }

  cancalRegisterMode(registerMode: boolean): void {
    this.registerMode = false;
  }

}
