import { AuthService } from './../_services/auth.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../_model/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private route: Router) { }
  user: User;
  registerForm: FormGroup;
  @Output() cancelRegister = new EventEmitter();
  ngOnInit(): void {
    this.createRegisterForm();
  }

  createRegisterForm(): void {
    this.registerForm = this.fb.group({
      gender: ['', Validators.required],
      age: ['', Validators.required],
      country: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(4)]],
      confirmPassword: ['', Validators.required]
    }, { validators: this.checkPassword });
  }

  checkPassword(fg: FormGroup): {mismatch: true} {
    return fg.get('password').value === fg.get('confirmPassword').value ? null : { mismatch: true };
  }

  register(): void {
    if (this.registerForm.valid) {
      this.user = Object.assign({}, this.registerForm.value);
      this.authService.register(this.user).subscribe(() => {
        // this.alerttify.success('register successful');
        console.log('register successful');
      }, error => {
        // this.alerttify.error(error);
        console.log(error);
      }, () => {
        this.authService.login(this.user).subscribe(() => {
          this.route.navigate(['/']);
        });
      });
    }
    console.log(this.registerForm.value);
  }

  cancle(): void {
    this.cancelRegister.emit(false);
    console.log('Cancelled');
  }
}
