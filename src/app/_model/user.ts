export interface User {
    id: number;
    email: string;
    age: number;
    gender: string;
    photoUrl: string;
    country: string;
}
